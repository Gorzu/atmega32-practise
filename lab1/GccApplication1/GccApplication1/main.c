/*
 * GccApplication1.c
 *
 * Created: 31/10/2020 7:12:22 pm
 * Author : Piotr
 */ 

#include <avr/io.h>
#include <stdlib.h>
#include <string.h>
#include <util/delay.h>

#ifndef _BV
#define _BV(bit)				(1<<(bit))
#endif
#ifndef sbi
#define sbi(reg,bit)		reg |= (_BV(bit))
#endif

#ifndef cbi
#define cbi(reg,bit)		reg &= ~(_BV(bit))
#endif

#ifndef tbi
#define tbi(reg,bit)		reg ^= (_BV(bit))
#endif

#define 	bit_is_set(sfr, bit)   (_SFR_BYTE(sfr) & _BV(bit))
#define 	bit_is_clear(sfr, bit)   (!(_SFR_BYTE(sfr) & _BV(bit)))


void setLED(uint8_t led, uint8_t state) // funkcja do wlaczania/wylaczania ledow
{
	uint8_t number[] = {PD0, PD1, PD2, PD3, PD4, PD5, PD6, PD7};

	if(led>0 && led<9)
	{
		if(state==1)
		{
			sbi(PORTD, number[led-1]);
			_delay_ms(200);
		}

		if(!state)
		{
			cbi(PORTD, number[led-1]);
			_delay_ms(200);
		}
	}
}



void changeLED(uint8_t led) // funkcja do zmiany stanu LEDow
{
	uint8_t number[] = {PD0, PD1, PD2, PD3, PD4, PD5, PD6, PD7};

	if(led>0 && led<9)
	{
		tbi(PORTD, number[led-1]);
		_delay_ms(200);
	}
}


int getButton() // Funkcja odczytujaca ktory przycisk zostal wcisniety
{
	for(int i=1; i<=4; i++)
	{
		if(i==1) PORTC = 0x80;
		if(i==2) PORTC = 0x40;
		if(i==3) PORTC = 0x20;
		if(i==4) PORTC = 0x10;

		
		if(bit_is_set(PINC, PC0))
			return i;
		
		if(bit_is_set(PINC, PC1))
			return i+4;
		
		if(bit_is_set(PINC, PC2))
			return i+8;
		
		if(bit_is_set(PINC, PC3))
			return i+12;
	}
	return -1;  // nic nie zostalo wcisniete

}



int main()
{
	DDRD = 0xFF;	// Port D na wyjscie
	
	setLED(getButton(), 1);	//pobranie przycisku z klawiatury, wlczenie diody o tym numerze
	int sw2_counter = 0;    // zmienna do zliczania wcisniec SW2


	while(1)
	{
		
		// ZADANIE 1 
		if(getButton()==1)		//wlaczenie diody po wcisnieciu SW1
			setLED(1,1);
			
		if(getButton()==5)		//wylaczenie diody po wcisnieciu SW5
			setLED(1,0);
		
		// ZADANIE 2 
		if(getButton()==3)		// zmiana stanu diody po wcisnieciu SW3
		{
			changeLED(1);
			_delay_ms(100);   // ustanie drgan
		}
		
		// ZADANIE 3 
		if(getButton()==2)		//wlaczenie diody po wcisnieciu SW1
		{
			if(sw2_counter <= 10)			
			{
			sw2_counter++;
			PORTD = 0x00;
			setLED(sw2_counter,1); 
			}
			
			else
			{
				PORTD = 0x00;
				sw2_counter = 1;
				setLED(sw2_counter,1); 
			}
		
		_delay_ms(100); 
		}
				
		/*
		// ZADANIE 4
		// Nale?y je odkomentowac i zakomentowac wszystkie powyzsze zadania w petli while(1)
		
		changeLED(getButton());
		_delay_ms(100); 
		*/
	}
}


