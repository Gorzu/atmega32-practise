/*
 * GccApplication1.c
 *
 * Created: 10.01.2021 14:08:48
 * Author : Piotr
 */ 

//#include "I2C_Master.h"
#define F_CPU 1000000UL  // 1 MHz
#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

void delay_ms(int ms){
	volatile long unsigned int i;
	for(i=0;i<ms;i++)
	_delay_ms(1);
}

void delay_us(int us){
	volatile long unsigned int i;
	for(i=0;i<us;i++)
	_delay_us(1);
}

#define RS 0
#define RW 1
#define E  2

void LCD2x16_init(void)
{
	PORTA &= ~(1<<RS);
	PORTA &= ~(1<<RW);

	PORTA |= (1<<E);
	PORTD = 0x38;   // dwie linie, 5x7 punktow
	PORTA &=~(1<<E);
	_delay_us(120);

	PORTA |= (1<<E);
	PORTD = 0x0e;   // wlacz wyswietlacz, kursor, miganie
	PORTA &=~(1<<E);
	_delay_us(120);

	PORTA |= (1<<E);
	PORTD = 0x06;
	PORTA &=~(1<<E);
	_delay_us(120);
}

void LCD2x16_clear(void){
	PORTA &= ~(1<<RS);
	PORTA &= ~(1<<RW);

	PORTA |= (1<<E);
	PORTD = 0x01;
	PORTA &=~(1<<E);
	delay_ms(120);
}

void LCD2x16_putchar(int data)
{
	PORTA |= (1<<RS);
	PORTA &= ~(1<<RW);

	PORTA |= (1<<E);
	PORTD = data;
	PORTA &=~(1<<E);
	_delay_us(120);
}

void LCD2x16_pos(int wiersz, int kolumna)
{
	PORTA &= ~(1<<RS);
	PORTA &= ~(1<<RW);

	PORTA |= (1<<E);
	delay_ms(1);
	PORTD = 0x80+(wiersz-1)*0x40+(kolumna-1);
	delay_ms(1);
	PORTA &=~(1<<E);
	_delay_us(120);
}


/////// Konwerter BCD -> Decimal //////////
unsigned char Konwerter(unsigned char bcd)
{
	unsigned char dec;
	dec = bcd >> 4;
	return(dec=dec*10+(bcd&=0x0F));
}


//////////////// RTC /////////////////////
int sekundy, minuty, godziny;

/* Inicjalizacja poczatkowych ustawien zegara */
             /* DATA WRITE */
void RTC_Init()
{
	twistart(); // start komunikacji z I2C	
	twiwrite(0b11010000); // Slave Address
	twiwrite(0); // Word Address
	
	twiwrite(0x00); // sekundy (00)
	twiwrite(0x30); // minuty (30)
	twiwrite(0x17); // godziny (17)
	
	twistop();			// zakonczenie komunikacji z I2C
}


/* Funkcja do odczytywania aktualnej godziny w nieskonczonej petli */
                        /* DATA READ */
void RTC_current()
{
	twistart(); // start komunikacji z I2C
	twiwrite(0b11010000); // Slave Address
	twiwrite(0); // 0 - odczytujemy od sekund	
	
	twistart(); // Repeated Start
	twiwrite(0b11010001); // Slave Address 
	sekundy = (int) Konwerter(twiread(1)); // sekundy
	minuty = (int) Konwerter(twiread(1)); // minuty
	godziny = (int) Konwerter(twiread(1)); // godziny
	twiread(0); // koniec odczytu
	twistop(); // koniec komunikacji z I2C	
}


/////////////// MAIN /////////////////////

int main(void){
	
	char autor[15] = "Piotr Gorzelnik";
	char cwiczenie[15] = "Real Time Clock";
	char tmp[16];
	int i=0;

	 DDRD = 0xff;
	 PORTD = 0x00;
	 DDRA = 0xff;
	 PORTA = 0x00;

	RTC_Init(); // inicjalizacja poczatkowej godziny i daty

	LCD2x16_init(); // inicjalizacja wyswietlacza LCD
	LCD2x16_clear(); // wyczyszczenie LCD

	// Podpis na poczatku dzialania programu //
	for(i=0; i<15; i++) LCD2x16_putchar(autor[i]);	
	LCD2x16_pos(2,1);
	for(i=0;i < 15; i++) LCD2x16_putchar(cwiczenie[i]);
	delay_ms(500);
	LCD2x16_clear();
	
	delay_ms(2000);
	LCD2x16_clear();
 
	 while(1)
	 {
		 RTC_current();
		 	 
		 sprintf(tmp,"Zegar:");
		 for(i=0; i<6; i++) LCD2x16_putchar(tmp[i]);	 
		 
		 LCD2x16_pos(2,1); // w drugiej linii zegar
		  sprintf(tmp,"%d:%d:%d", godziny, minuty, sekundy);
		  for(i=0; i<8; i++) LCD2x16_putchar(tmp[i]);
		 	
		 delay_ms(1000);
		 LCD2x16_clear();
	 }

	 return 0;	
}

