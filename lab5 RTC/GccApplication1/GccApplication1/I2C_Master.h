/*
 * I2C_Master.h
 *
 * Created: 10.01.2021 18:19:47
 *  Author: Piotr
 */ 

#ifndef I2C_MASTER_H_
#define I2C_MASTER_H_

#define F_CPU 8000000UL  // 8 MHz
#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>												

void twistart(void); // I2C funkcja startu
void twistop(void);	 // I2C funkcja stop
void twiwrite(unsigned char data);   // I2C funkcja pisania
int twiread(int ack);  // I2C funkcja odczytu

#endif /* I2C_MASTER_H_ */