/*
 * I2C_Master.c
 *
 * Created: 10.01.2021 18:22:14
 *  Author: Piotr
 */ 

#include "I2C_Master.h"				


/* Start komunikacji */
void twistart(void){
	TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN); // zezwol na TWI, wygenerowanie wartosci startowej i wyczyszczenie przerwan
	while(!(TWCR&(1<<TWINT))); // czekaj az TWI zakonczy aktualne zadanie (start)
}
/* Zakonczenie komunikacji */
void twistop(void){ //bit stopu
	TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWSTO); // zezwol na TWI, wygenerowanie wartosci stop i wyczyszczenie przerwan
	while((TWCR&(1<<TWSTO))); // czekaj az TWI zakonczy aktualne zadanie (stop)
}
/* Zapis czasu */
void twiwrite(unsigned char data){
	TWDR = data; // skopiuj dane z rejestru TWI
	TWCR = (1<<TWINT)|(1<<TWEN); // zezwolenie na TWI i wyczysczenie przerwan
	while(!(TWCR&(1<<TWINT))); // czekaj az TWI zakonczy aktualne zadanie (wpisywanie)
}

/* Odczyt czasu */
int twiread (int ack){
	TWCR=(ack ?
	((1<< TWINT)|(1<<TWEN)|(1<<TWEA)) // zezwol na TWI, wygeneruj ack i wyczysc przerwania
	:
	((1<<TWINT)|(1<<TWEN))); // jesli ack!=0 to jedynie zezwol na TWI i wyczysc przerwania
	while(!(TWCR&(1<<TWINT))); // czekaj az TWI zakonczy aktualne zadanie (odczytywanie)
	return TWDR; // zwroc odczytane dane
}




