/*
 * GccApplication1.c
 *
 * Created: 13/12/2020 8:31:59 pm
 * Author : Piotr
 */ 

#define F_CPU 8000000UL // czestotliwosc taktowania zegara 8MHz
#include <avr/io.h>
#include <stdlib.h>
#include <string.h>
#include <util/delay.h>
#include <avr/interrupt.h>

uint8_t tab[] = {0b00001000, 0b00001100, 0b00000100, 0b00000110, 0b00000011, 0b00000001, 0b00001001}; // tablica uzwojen
int counter = 0;

int main(void)
{
    DDRA=0xFF; // Port A na wyjscie
	
    while (1) // ZADANIE 3
    {	
		PORTA = tab[counter%8];
		counter++;
		_delay_ms(2);
    }
}

