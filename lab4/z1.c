/*
 * GccApplication1.c
 *
 * Created: 13/12/2020 8:31:59 pm
 * Author : Piotr
 */ 

#define F_CPU 8000000UL // czestotliwosc taktowania zegara 8MHz
#include <avr/io.h>
#include <stdlib.h>
#include <string.h>
#include <util/delay.h>
#include <avr/interrupt.h>

int main(void)
{
    DDRA=0xFF; // Port A na wyjscie
	
    while (1) // ZADANIE 1
    {
		PORTA = 0b00000001; // 1
		_delay_ms(2);
		PORTA = 0b00000010; // 2
		_delay_ms(2);
		PORTA = 0b00000100; // 4
		_delay_ms(2);
		PORTA = 0b00001000; // 8
		_delay_ms(2);
		
    }
}

