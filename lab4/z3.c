/*
 * GccApplication1.c
 *
 * Created: 13/12/2020 8:31:59 pm
 * Author : Piotr
 */ 

#define F_CPU 8000000UL // czestotliwosc taktowania zegara 8MHz
#include <avr/io.h>
#include <stdlib.h>
#include <string.h>
#include <util/delay.h>
#include <avr/interrupt.h>

int main(void)
{
    DDRA=0xFF; // Port A na wyjscie
	
    while (1) // ZADANIE 3
    {	
		PORTA = 0b00000011;
		_delay_ms(2);
		PORTA = 0b00000110;
		_delay_ms(2);
		PORTA = 0b00001100;
		_delay_ms(2);
		PORTA = 0b00001001;
		_delay_ms(2);
    }
}

