/*
 * GccApplication1.c
 *
 * Created: 13/12/2020 8:31:59 pm
 * Author : Piotr
 */ 

#define F_CPU 8000000UL // czestotliwosc taktowania zegara 8MHz
#include <avr/io.h>
#include <stdlib.h>
#include <string.h>
#include <util/delay.h>
#include <avr/interrupt.h>

uint8_t tab[] = {0b00001000, 0b00001100, 0b00000100, 0b00000110, 0b00000011, 0b00000001, 0b00001001}; // tablica uzwojen
int counter = 0; 

// Obsluga przerwan timera T0
ISR(TIMER0_COMP_vect)
{
	if(counter>=7){counter=0;}
	PORTA = tab[counter];
	counter++;
}

// Inicjalizacja timera T0
void timerT0_setup(void){
	TCCR0 = (1<<WGM01); // ustawienie licznika do pracy w trybie CTC
	TCCR0 |= (1<<CS02) | (1<<CS00); // preskaler 1024
	TCNT0 = 0; // wyzerowanie licznika
	OCR0 = 40; // wartosc top
	TIMSK = (1<<OCIE0); // przerwanie gdy dojdzie do wartosci top
}

int main(void)
{
	DDRA=0xFF;
	timerT0_setup(); // inicjalizacja timera T0
	sei(); // zezwolenie na globalne przerwania
	
	while (1){	
	}
}

