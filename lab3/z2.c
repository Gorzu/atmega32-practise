/*
 * PTM2_lab3_timery.c
 *
 * Created: 28/11/2020 3:54:58 pm
 * Author : Piotr
 */ 
#define F_CPU 8000000UL // czestotliwosc taktowania zegara 8MHz
#include <avr/io.h>
#include <stdlib.h>
#include <string.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#ifndef _BV
#define _BV(bit)				(1<<(bit))
#endif
#ifndef sbi
#define sbi(reg,bit)		reg |= (_BV(bit))
#endif

#ifndef cbi
#define cbi(reg,bit)		reg &= ~(_BV(bit))
#endif

#ifndef tbi
#define tbi(reg,bit)		reg ^= (_BV(bit))
#endif

#define 	bit_is_set(sfr, bit)   (_SFR_BYTE(sfr) & _BV(bit))
#define 	bit_is_clear(sfr, bit)   (!(_SFR_BYTE(sfr) & _BV(bit)))

volatile uint8_t LED[]={PA0,PA1,PA2,PA3,PA4,PA5}; // porty ledow na pasku diodowym

// Inicjalizacja timera T0 
void timerT0_setup(void){
	TCCR0 = (1<<COM00); // ustawienie licznika do pracy w trybie normalnym
	TCCR0 |= (1<<CS02) | (1<<CS01); // licznik zlicza opadajace zbocza na wyprowadzeniu T0
	TCNT0=0; // wyzerowanie licznika opadajacych zboczy
	OCR0=10; // gdy TCNT0 bedzie rowne to ma sie wyzerowac
	sbi(TIMSK, OCIE0); // przerwanie gdy dojdzie do 10
}

int main(void){
	DDRA=0xFF; // Port A na wyjscie (linijka LED)
	PORTA = 0xFF; // ledy na stan niski
	PORTB = 0b00000001; // pull-up
	timerT0_setup(); // inicjalizacja timera T0	
	sei(); // zezwolenie na globalne przerwania
	
	 while (1){
		 tbi(PORTA, LED[TCNT0%6]);
	 }
} 

