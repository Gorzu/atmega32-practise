/*
 * PTM2_lab3_timery.c
 *
 * Created: 28/11/2020 3:54:58 pm
 * Author : Piotr
 */ 
#define F_CPU 8000000UL // czestotliwosc taktowania zegara 8MHz
#include <avr/io.h>
#include <stdlib.h>
#include <string.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#ifndef _BV
#define _BV(bit)				(1<<(bit))
#endif
#ifndef sbi
#define sbi(reg,bit)		reg |= (_BV(bit))
#endif

#ifndef cbi
#define cbi(reg,bit)		reg &= ~(_BV(bit))
#endif

#ifndef tbi
#define tbi(reg,bit)		reg ^= (_BV(bit))
#endif

#define 	bit_is_set(sfr, bit)   (_SFR_BYTE(sfr) & _BV(bit))
#define 	bit_is_clear(sfr, bit)   (!(_SFR_BYTE(sfr) & _BV(bit)))

volatile uint8_t LED[]={PA0,PA1,PA2,PA3,PA4,PA5}; // porty ledow na pasku diodowym

// Inicjalizacja timera T0
void timerT0_setup(void){
	TCCR0 = (1<<CS00) | (1<<CS02); // preskaler 1024
	TCCR0 |= (1<<WGM00) | (1<<WGM01); // tryb FAST PWM
	TCCR0 |= (COM01); // overflow, OC0 do wartosci bottom
	TCNT0=0; // wyzerowanie licznika
	TIMSK = TOIE0; // przepelnienie poskutkuje przerwaniem
}

int main(void){
	sbi(DDRB, PB3);
	cbi(PORTB, PB3); // wyprowadzenie OC0 na wyj?cie

	timerT0_setup(); // inicjalizacja timera T0
	sei(); // zezwolenie na globalne przerwania
	
	while (1){
		OCR0 = 255*0.9; // wype?nienie 90%
		//OCR0 = 255*0.05; // wype?nienie 5%
	}
}

