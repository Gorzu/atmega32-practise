/*
 * laboratorium6.c
 *
 * Created: 25.01.2021 18:26:01
 * Author : Piotr
 */ 

#define F_CPU 8000000UL  // 8 MHz
#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>
#include <string.h>

void delay_ms(int ms)
{
	volatile long unsigned int i;
	for(i=0;i<ms;i++)
	_delay_ms(1);
}

void delay_us(int us)
{
	volatile long unsigned int i;
	for(i=0;i<us;i++)
	_delay_us(1);
}

//RS PA0
//RW PA1
//E  PA2
//DATA PD

#define RS 0
#define RW 1
#define E  2

void LCD2x16_init(void)
{
	PORTA &= ~(1<<RS);
	PORTA &= ~(1<<RW);

	PORTA |= (1<<E);
	PORTD = 0x38;   // dwie linie, 5x7 punktow
	PORTA &=~(1<<E);
	_delay_us(120);

	PORTA |= (1<<E);
	PORTD = 0x0e;   // wlacz wyswietlacz, kursor, miganie
	PORTA &=~(1<<E);
	_delay_us(120);

	PORTA |= (1<<E);
	PORTD = 0x06;
	PORTA &=~(1<<E);
	_delay_us(120);
}

void LCD2x16_clear(void){
	PORTA &= ~(1<<RS);
	PORTA &= ~(1<<RW);

	PORTA |= (1<<E);
	PORTD = 0x01;
	PORTA &=~(1<<E);
	delay_ms(120);
}

void LCD2x16_putchar(int data)
{
	PORTA |= (1<<RS);
	PORTA &= ~(1<<RW);

	PORTA |= (1<<E);
	PORTD = data;
	PORTA &=~(1<<E);
	_delay_us(120);
}

void LCD2x16_pos(int wiersz, int kolumna)
{
	PORTA &= ~(1<<RS);
	PORTA &= ~(1<<RW);

	PORTA |= (1<<E);
	delay_ms(1);
	PORTD = 0x80+(wiersz-1)*0x40+(kolumna-1);
	delay_ms(1);
	PORTA &=~(1<<E);
	_delay_us(120);
}


void Instrukcja(uint8_t db, uint8_t rs)
{
	if(rs){
			PORTA |= (1<<RS);
		 }
	
	else if(rs == 0){
			 PORTA &=~(1<<RS);
		 }

	PORTA &= ~(1<<RW); // odczyt danych
	PORTA |= (1<<E); // PORTA na stan wysoki
	PORTD = db; // wykonanie wprowadzonej instrukcji
	PORTA &=~(1<<E); // PORTA na stan niski
	delay_ms(2);
}


void SymbolInit(){
	PORTA |= (1<<RS);
	PORTA &= ~(1<<RW);
	PORTA |= (1<<E);
}


int main(void){
	DDRD = 0xff;
	PORTD = 0x00;
	DDRA = 0xff;
	PORTA = 0x00;

	_delay_ms(200);
	LCD2x16_init();
	LCD2x16_clear();

	Instrukcja(0b01000000, 0); // ustawienie adresu CGRAM
	
	/* Symbol dzwonka */
	Instrukcja(0b00000100, 1);
	Instrukcja(0b00001110, 1);
	Instrukcja(0b00001110, 1);
	Instrukcja(0b00001110, 1);
	Instrukcja(0b00011111, 1);
	Instrukcja(0b00000100, 1);
	Instrukcja(0b00000000, 1);
	Instrukcja(0b00000000, 1);

	/* Symbol usmiechnietej twarzy */
	Instrukcja(0b00000000, 1);
	Instrukcja(0b00001010, 1);
	Instrukcja(0b00000000, 1);
	Instrukcja(0b00000100, 1);
	Instrukcja(0b00010001, 1);
	Instrukcja(0b00001110, 1);
	Instrukcja(0b00000000, 1);

	
	LCD2x16_pos(1,1);
	SymbolInit(); // ustawienie portow
	PORTD = 0b00000000; // pierwszy symbol
	PORTA &= ~(1<<E); // stan niski
	_delay_ms(100);
	
	Instrukcja(0b00010100,0); // przesuniecie kursora w prawo
	SymbolInit();
	PORTD = 0b00000001; // drugi symbol
	PORTA &= ~(1<<E); // stan niski
		
	while(1){	
	}
	return 0;
}

