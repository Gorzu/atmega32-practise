/*
 * GccApplication1.c
 *
 * Created: 16/11/2020 3:46:46 pm
 * Author : Piotr
 */ 
#include <avr/io.h>
#include <stdlib.h>
#include <string.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#ifndef _BV
#define _BV(bit)				(1<<(bit))
#endif
#ifndef sbi
#define sbi(reg,bit)		reg |= (_BV(bit))
#endif

#ifndef cbi
#define cbi(reg,bit)		reg &= ~(_BV(bit))
#endif

#ifndef tbi
#define tbi(reg,bit)		reg ^= (_BV(bit))
#endif

#define 	bit_is_set(sfr, bit)   (_SFR_BYTE(sfr) & _BV(bit))
#define 	bit_is_clear(sfr, bit)   (!(_SFR_BYTE(sfr) & _BV(bit)))

volatile uint8_t ledCounter=0; // licznik potrzebny do zapalania odpowiedniej liczby ledow
volatile uint8_t number[] = {PA0, PA1, PA2, PA3, PA4, PA5, PA6}; // porty ledow

	ISR(INT0_vect){ // Narastajjce zbocze na INT0
		PORTA = 0x00;		
	}

	ISR(INT1_vect){ // opadajace zbocze na INT1;
		PORTA = 0xFF;
	}

int main(void)
{
  DDRA = 0xFF;	// Port A na wyjscie
  // konfiguracja zglaszania przerwan
  MCUCR |= (1<<ISC11) | (0<<ISC10) | (1<<ISC01) | (1<<ISC00);
  MCUCSR |= (1<<ISC2);
  
  GICR |= (1<<INT0) | (1<<INT1) | (1<<INT2); // zezwolenie na wystapienie przerwan
  sei();  // wlaczenie przerwan 
	
    while (1){						
    }
}

